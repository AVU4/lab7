#include "mem.h"
#include <errno.h>
#include <string.h>

#define MIN_SIZE 4096 
#define MMAP_PROT (PROT_READ | PROT_WRITE)
#define MMAP_FLAGS (0x02 | 0x20)


static void* startAddress;
static void* endAddress;


static struct mem* get_last(void) {
	struct mem* head = (struct mem*) startAddress;
	while(head->next != NULL){
		head = head->next;
	}
	return head;
}

static size_t get_current_size(size_t query){
	query = query > MIN_SIZE ? query : MIN_SIZE;
	query = query % 4096 == 0 ? query : query + (4096 - query%4096);
	return query;
}


static struct mem* get_space(struct mem* head, size_t query) {

	while (head != NULL){
		if (head->is_free == 1 && head->capacity >= query){
			return head;
		}
		head = head->next;
	}
	return NULL;
}


void* heap_init(size_t initial_size) { 
	struct mem* head;
	void* pointer;
	initial_size = get_current_size(initial_size); 

	pointer = mmap(NULL, initial_size + 1,
			MMAP_PROT,
			MMAP_FLAGS,
			-1, 0);

	if (!startAddress){
		startAddress = pointer;
	}
	endAddress = (char*) pointer + initial_size;
	head = (struct mem*) pointer;
	head->next = NULL;
	head->capacity = initial_size - sizeof(struct mem);
	head->is_free = 0;

	printf("Создана куча размером %ld по адресу %p\n", initial_size, (void*) head);
	return head;	
}

struct mem* get_block(struct mem* head, size_t query){
	char* pointer;
	struct mem* other_space;
	if (head->capacity - query <= sizeof(struct mem)){
		printf("Заполнен целый блок по адресу %p размер блока %ld\n", (void*) head, head->capacity);
		head->is_free = 0;
	       	return head;
	}
	pointer = (char*) head + sizeof(struct mem) + query;
       	other_space = (struct mem*) pointer;
	other_space->capacity = head->capacity - query;
	other_space->is_free = 1;
	other_space->next = NULL;
	head->next  = other_space;
	head->is_free = 0;
	head->capacity = query;
	printf("Удалось разбить кучу на два участка %p и %p размером %ld и %ld соответственно\n", (void*) head, (void*) other_space, head->capacity, other_space->capacity);
	return head;
}

void* _malloc(size_t query) {
	struct mem* head;
	
	if (startAddress == NULL) {
		head = (struct mem*) heap_init(query + sizeof(struct mem));
		head = get_block(head, query);
		return (char*) head + sizeof(struct mem);
	}else{
		struct mem* block;
		
		head = (struct mem*) startAddress;
		block = get_space(head, query + sizeof(struct mem));

		if (block != NULL) {
			block = get_block(block, query);
			return (char*) block + sizeof(struct mem);
		}else{
			char* pointer;
			char* tmp;
			size_t rest;
			struct mem* new_block;


			block = get_last();
			if (block->is_free == 1){
				pointer =(char*) endAddress;
				rest = query + sizeof(struct mem) - block->capacity;
				rest = get_current_size(rest);
				
				tmp = (char*) mmap(pointer, rest + 1,
					       MMAP_PROT,
					       MAP_FIXED | MMAP_FLAGS,
					       -1, 0);

				if (tmp != (void*) -1 && tmp != NULL) {
					printf("Произошло увеличение кучи%s\n", "");
					endAddress = pointer + rest;
					block->capacity = block->capacity + rest;
					block = get_block(block, query);
					return (char*) block + sizeof(struct mem);
				}

			}
			new_block = (struct mem*) heap_init(sizeof(struct mem) + query);
			if (new_block == NULL) return NULL;
			new_block = get_block(new_block, query);
			block->next = new_block;
			return (char*) new_block + sizeof(struct mem);

		}


	}


}

void _free(void* mem){
	struct mem* memory = (struct mem*) startAddress;
	char* pointer = (char*) startAddress;
	while (pointer + sizeof(struct mem) != mem && pointer != NULL) {
		memory = memory->next;
		pointer = (char*) memory->next;
	}
	while(memory!= NULL){
		printf("Освобождения участка %p\n", (void*) memory);
		memory->is_free = 1;
		if (memory->next != NULL && memory->next->is_free == 1 && ((char*) memory + sizeof(struct mem) + memory->capacity == (char*) memory->next)){
			printf("Объединение свободных участков %p и %p\n", (void*) memory, (void*) memory->next);
			memory->capacity = memory->capacity + sizeof(struct mem) + memory->next->capacity;
			memory->next = memory->next->next;
			memory = memory->next;
		}else{
			break;
		}
		
	}
}



